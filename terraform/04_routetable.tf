resource "aws_route_table" "rt_sysadmin" {
	vpc_id="${aws_vpc.vpc_sysadmin.id}"
	route {
		cidr_block="0.0.0.0/0"
		gateway_id="${aws_internet_gateway.gw_sysadmin.id}"
	}
}
resource "aws_route_table_association" "subnet_table_sysadmin" {
	subnet_id="${aws_subnet.subnet_sysadmin.id}"
	route_table_id="${aws_route_table.rt_sysadmin.id}"
}

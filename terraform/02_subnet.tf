resource "aws_subnet" "subnet_sysadmin" {
	vpc_id = "${aws_vpc.vpc_sysadmin.id}"
	cidr_block = "10.0.10.0/24"
	map_public_ip_on_launch = true
	availability_zone = "us-east-1a"
	tags = {
		Name = "Subnet SysAdmin"
	}
}

resource "aws_security_group" "sg_sysadmin_1" {
    name = "sg_sysadmin"
    vpc_id = "${aws_vpc.vpc_sysadmin.id}"
    description = "Permitir ssh y https"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 8443
        to_port = 8443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags = {
        Name = "SSH y HTTPS/TCP"
    }
}

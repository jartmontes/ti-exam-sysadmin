resource "aws_key_pair" "key_sysadmin_1" {
	key_name = "key_sysadmin"
	public_key = "${file("key_sysadmin.pem.pub")}"
} 

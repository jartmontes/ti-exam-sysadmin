resource "aws_instance" "web_sysadmin" {
   ami = "ami-0ba62214afa52bec7"  
   instance_type = "t2.micro"
   count = 1
   subnet_id = "${aws_subnet.subnet_sysadmin.id}"
   associate_public_ip_address = true
   vpc_security_group_ids = ["${aws_security_group.sg_sysadmin_1.id}"]
   private_ip = "10.0.10.10"
   key_name = ""

   tags = {
      Name = "web_sysadmin"
      Owner = "terraform"
      Env = "prod"
   }
}

resource "aws_internet_gateway" "gw_sysadmin" {
	vpc_id = "${aws_vpc.vpc_sysadmin.id}"
	tags = {
		Name = "Gateway SysAdmin"
	}
}
